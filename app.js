const AWS = require("aws-sdk");
const SES = new AWS.SES({ region: "us-west-2" });

const FROM_EMAIL_ADDRESS = process.env.FROM_EMAIL_ADDRESS;
const TO_EMAIL_ADDRESS = process.env.TO_EMAIL_ADDRESS;

function send400(text) {
  return {
    statusCode: 400,
    body: `Bad Request${text ? ` - ${text}` : ""}`,
  };
}

function send(event) {
  const { subject, text, to } = event;

  if (!subject || !text) return send400("Subject and Text required");

  // if to address customization needed, uncomment below and send an array in
  // the json (ex "to": ["scottsanders25@gmail.com", "scott@scottsandersdev.com"]) - 
  // const toAddresses = to && to.length ? [...to] : [TO_EMAIL_ADDRESS];
  const toAddresses = [TO_EMAIL_ADDRESS];

  const email = {
    Destination: {
      ToAddresses: toAddresses,
    },
    Message: {
      Body: {
        Html: {
          Data: text,
        },
      },
      Subject: {
        Data: subject || "Request from Flueid.com",
      },
    },
    Source: FROM_EMAIL_ADDRESS,
    ReplyToAddresses: [FROM_EMAIL_ADDRESS],
  };


  return SES.sendEmail(email)
    .promise()
    .then((data) => {
      console.log(
        "[CommunicationsReceiver - send]: email sent. Message ID:",
        data.MessageId
      );
      return {
        statusCode: 200,
        body: JSON.stringify(data),
      };
    })
    .catch((err) => {
      console.error("[CommunicationsReceiver - send]: email error:", err.stack);
      return {
        statusCode: 500,
        body: JSON.stringify(err),
      };
    });
}

exports.lambdaHandler = async (event, context, callback) => {
  console.log(
    "[CommunicationsReceiver - event received]: ",
    JSON.stringify(event)
  );
  return send(event);
};
